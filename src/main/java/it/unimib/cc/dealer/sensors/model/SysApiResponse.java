package it.unimib.cc.dealer.sensors.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class SysApiResponse {

	@JsonProperty("type")
	private Integer type = null;

	@JsonProperty("id")
	private Long id = null;

	@JsonProperty("message")
	private String message = null;

	@JsonProperty("country")
	private String country = null;

	@JsonProperty("sunrise")
	private Long sunrise = null;

	@JsonProperty("sunset")
	private Long sunset = null;
}
