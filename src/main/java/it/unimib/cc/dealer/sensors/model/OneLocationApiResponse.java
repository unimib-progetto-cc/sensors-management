package it.unimib.cc.dealer.sensors.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class OneLocationApiResponse {

	@JsonProperty("coord")
	private CoordApiResponse coord = null;

	@JsonProperty("weather")
	private List<WeatherApiResponse> weathers = null;

	@JsonProperty("base")
	private String base = null;

	@JsonProperty("main")
	private MainApiResponse main = null;

	@JsonProperty("visibility")
	private Integer visibility = null;

	@JsonProperty("wind")
	private WindApiResponse wind = null;

	@JsonProperty("rain")
	private RainApiResponse rain = null;

	@JsonProperty("clouds")
	private CloudsApiResponse clouds = null;

	@JsonProperty("snow")
	private SnowApiResponse snow = null;

	@JsonProperty("dt")
	private Long datetime = null;

	@JsonProperty("sys")
	private SysApiResponse sys = null;

	@JsonProperty("timezone")
	private Integer timezone = null;

	@JsonProperty("id")
	private Long id = null;

	@JsonProperty("name")
	private String name = null;

	@JsonProperty("cod")
	private Integer cod = null;
}
