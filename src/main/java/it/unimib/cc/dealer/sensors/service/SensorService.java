package it.unimib.cc.dealer.sensors.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import it.unimib.cc.dealer.sensors.exception.ResourceNotFoundException;
import it.unimib.cc.dealer.sensors.factory.WeatherFactory;
import it.unimib.cc.dealer.sensors.model.Position;
import it.unimib.cc.dealer.sensors.model.Sensor;
import it.unimib.cc.dealer.sensors.model.Sensors;
import it.unimib.cc.dealer.sensors.model.Weather;
import it.unimib.cc.dealer.sensors.repository.SensorRepository;

@Service
public class SensorService {

    @Autowired
    private SensorRepository repository;

    @Autowired
    private ApiService apiService;

    public Sensor findById(long id) {
    	return repository.findById(id)
			.orElseThrow(() -> 
				new ResourceNotFoundException("Sensor not found with id " + id)
			);
    }

    @HystrixCommand(fallbackMethod = "saveWithoutWeather")
    public Sensor save(Sensor sensor) {
    	Weather weather = WeatherFactory.responseToModel(apiService.oneLocationWeather(sensor.getPosition()));
    	sensor.setDatetime(LocalDateTime.now());
    	sensor.setWeather(weather);
    	return repository.save(sensor);
    }

    @SuppressWarnings("unused")
	private Sensor saveWithoutWeather(Sensor sensor) {
    	sensor.setDatetime(LocalDateTime.now());
    	return repository.save(sensor);
    }
    
    public Position findLasPositionByCarId (long carId) {
    	return repository.findTopByCarIdOrderByDatetimeDesc(carId)
    			.map(lastSensor -> {
    				return lastSensor.getPosition();
    			})
    			.orElseThrow(() -> 
    				new ResourceNotFoundException("Sensor not found with car_id " + carId)
    			);
    }
    
    public Sensors findSensorsByFields(Long carId, LocalDateTime startDatetime, LocalDateTime endDatetime) {
    	if (carId == null) {	//without carId
    		if (startDatetime != null) {
    			if (endDatetime != null) {	//range between start and end date without carId
    				return new Sensors (repository.findByDatetimeBetween(startDatetime, endDatetime));
    			}
    			else {	//from startDate without carId and endDate
    				return new Sensors (repository.findByDatetimeGreaterThanEqual(startDatetime));
    			}
    		}
    		else {
    			if (endDatetime != null) {	//until endDate without carId and startDate
    				return new Sensors (repository.findByDatetimeLessThanEqual(endDatetime));
    			}
    			else {	//all sensors
    				return new Sensors(repository.findAll());
    			}
    		}
    	}
    	else {
    		if (startDatetime != null) {
    			if (endDatetime != null) {	//range between start and end date with carId
    				return new Sensors (repository.findByCarIdAndDatetimeBetween(carId, startDatetime, endDatetime));
    			}
    			else {	//from startDate with carId, without endDate
    				return new Sensors (repository.findByCarIdAndDatetimeGreaterThanEqual(carId, startDatetime));
    			}
    		}
    		else {
    			if (endDatetime != null) {	//until endDate with carId, without startDate
    				return new Sensors (repository.findByCarIdAndDatetimeLessThanEqual(carId, endDatetime));
    			}
    			else {	//all sensors with carId
    				return new Sensors(repository.findByCarId(carId));
    			}
    		}
    	}
    }
}
