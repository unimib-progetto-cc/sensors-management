package it.unimib.cc.dealer.sensors.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@Embeddable
@JacksonXmlRootElement(localName = "weather")
public class Weather {

    @NotBlank
    @Column(nullable = false)
	private String description;

    @Column(nullable = false)
	private float temperature;

    @Column(nullable = false)
	private float pressure;

    @Column(nullable = false)
	private int humidity;

    @Column(nullable = false)
	private int visibility;
	
	private float windSpeed;
	
	private float rainVolumeLastHour;

    @Column(nullable = false)
	private int cloudiness;
	
	private float snowVolumeLastHour;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}

	public float getPressure() {
		return pressure;
	}

	public void setPressure(float pressure) {
		this.pressure = pressure;
	}

	public int getHumidity() {
		return humidity;
	}

	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}

	public int getVisibility() {
		return visibility;
	}

	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}

	public float getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(float windSpeed) {
		this.windSpeed = windSpeed;
	}

	public float getRainVolumeLastHour() {
		return rainVolumeLastHour;
	}

	public void setRainVolumeLastHour(float rainVolumeLastHour) {
		this.rainVolumeLastHour = rainVolumeLastHour;
	}

	public int getCloudiness() {
		return cloudiness;
	}

	public void setCloudiness(int cloudiness) {
		this.cloudiness = cloudiness;
	}

	public float getSnowVloumeLastHour() {
		return snowVolumeLastHour;
	}

	public void setSnowVloumeLastHour(float snowVloumeLastHour) {
		this.snowVolumeLastHour = snowVloumeLastHour;
	}

	@Override
	public String toString() {
		return "Weather [description=" + description + ", temperature=" + temperature + ", pressure="
				+ pressure + ", humidity=" + humidity + ", visibility=" + visibility + ", windSpeed=" + windSpeed
				+ ", rainVolumeLastHour=" + rainVolumeLastHour + ", cloudiness=" + cloudiness + ", snowVloumeLastHour="
				+ snowVolumeLastHour + "]";
	}

}
