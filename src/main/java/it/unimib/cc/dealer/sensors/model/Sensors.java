package it.unimib.cc.dealer.sensors.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "sensors")
public class Sensors extends RepresentationModel<Sensors> {

    @JacksonXmlProperty(localName = "sensor")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Sensor> sensors = new ArrayList<>();

	public Sensors(List<Sensor> sensors) {
		super();
		this.sensors = sensors;
	}

	public List<Sensor> getSensors() {
		return sensors;
	}

	public void setSensors(List<Sensor> sensors) {
		this.sensors = sensors;
	}

	@Override
	public String toString() {
		return "Sensors [sensors=" + sensors + "]";
	}
}
