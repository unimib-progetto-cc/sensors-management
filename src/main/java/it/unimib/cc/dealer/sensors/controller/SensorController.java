package it.unimib.cc.dealer.sensors.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.unimib.cc.dealer.sensors.model.Position;
import it.unimib.cc.dealer.sensors.model.Sensor;
import it.unimib.cc.dealer.sensors.model.Sensors;
import it.unimib.cc.dealer.sensors.security.SecurityUtils;
import it.unimib.cc.dealer.sensors.service.SensorService;
import it.unimib.cc.dealer.sensors.type.AppConst.CarsManagementMicroserviceEndpoint;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/sensors")
@Slf4j
public class SensorController {

    @Autowired
    private SensorService sensorService;
    
    @Autowired
    private Environment environment;

	//TODO: da fare
    @GetMapping(value = "",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Sensors getSensors(HttpServletRequest request,
    		@RequestParam(required = false) Long carId, 
    		@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime startDatetime, 
    		@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime endDatetime) 
    				throws NoSuchMethodException, SecurityException {
    	SecurityUtils.checkParameters(request, Arrays.asList(new String[]{"carId", "startDatetime", "endDatetime"}));
    	Sensors sensors = sensorService.findSensorsByFields(carId, startDatetime, endDatetime);
    	sensors.getSensors().forEach( (sensor) -> {
    		sensor.add(WebMvcLinkBuilder.linkTo(SensorController.class).slash(sensor.getId()).withSelfRel());
    		sensor.add(Link.of(
    				environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CAR_PREFIX) + sensor.getCarId()).
    				withRel("car"));
    		});
    	sensors.add(WebMvcLinkBuilder.linkTo(SensorController.class).withSelfRel());
		sensors.add(Link.of(
				environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CARS)).withRel("cars"));
        log.info("All sensors found: " + sensors);
        return sensors;
    }

    @PostMapping(value = "",
    		produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
    		consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Sensor> createSensor(HttpServletRequest request, @Valid @RequestBody Sensor sensor) {
    	SecurityUtils.checkParameters(request, new ArrayList<>());
    	Sensor createdSensor = sensorService.save(sensor);
  		sensor.add(WebMvcLinkBuilder.linkTo(SensorController.class).slash(createdSensor.getId()).withSelfRel());
		sensor.add(Link.of(
				environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CAR_PREFIX) + createdSensor.getCarId()).
				withRel("car"));
    	log.info("Sensor saved: " + createdSensor);
        return new ResponseEntity <Sensor> (createdSensor, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{sensorId}",  
    		produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Sensor getSensor(HttpServletRequest request, @PathVariable long sensorId) {
    	SecurityUtils.checkParameters(request, new ArrayList<>());
    	Sensor sensor = sensorService.findById(sensorId);
  		sensor.add(WebMvcLinkBuilder.linkTo(SensorController.class).slash(sensor.getId()).withSelfRel());
		sensor.add(Link.of(
				environment.getProperty(CarsManagementMicroserviceEndpoint.GET_CAR_PREFIX) + sensor.getCarId()).
				withRel("car"));
  		log.info("Sensor found: " + sensor);
  		return sensor;
    }

    @GetMapping(value = "/last-car-position/{carId}",  
    		produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Position getLastCarPosition(HttpServletRequest request, @PathVariable long carId) {
    	SecurityUtils.checkParameters(request, new ArrayList<>());
    	Position position = sensorService.findLasPositionByCarId(carId);
  		log.info("Position found: " + position);
  		return position;
    }
}
