package it.unimib.cc.dealer.sensors.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@Embeddable
@JacksonXmlRootElement(localName = "position")
public class Position {

    @Column(nullable = false)
	private float longitude;

    @Column(nullable = false)
	private float latitude;

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	@Override
	public String toString() {
		return "Position [longitude=" + longitude + ", latitude=" + latitude + "]";
	}
}
