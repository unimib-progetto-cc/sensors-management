package it.unimib.cc.dealer.sensors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@EnableCircuitBreaker
@SpringBootApplication
public class SensorsManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(SensorsManagementApplication.class, args);
	}

}
