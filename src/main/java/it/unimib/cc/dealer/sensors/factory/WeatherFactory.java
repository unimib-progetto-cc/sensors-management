package it.unimib.cc.dealer.sensors.factory;

import it.unimib.cc.dealer.sensors.model.OneLocationApiRequest;
import it.unimib.cc.dealer.sensors.model.OneLocationApiResponse;
import it.unimib.cc.dealer.sensors.model.Position;
import it.unimib.cc.dealer.sensors.model.Weather;

public class WeatherFactory {
	
	public static Weather responseToModel(OneLocationApiResponse oneLocation) {
		Weather weather = new Weather();
		weather.setDescription(oneLocation.getWeathers().get(0).getDescription());
		weather.setTemperature(oneLocation.getMain().getTemperature());
		weather.setPressure(oneLocation.getMain().getPressure());
		weather.setHumidity(oneLocation.getMain().getHumidity());
		weather.setVisibility(oneLocation.getVisibility());
		if (oneLocation.getWind() != null) {
			weather.setWindSpeed(oneLocation.getWind().getSpeed());
		}
		if (oneLocation.getRain() != null) {
			weather.setRainVolumeLastHour(oneLocation.getRain().getLast1Hour());
		}
		if (oneLocation.getClouds() != null) {
			weather.setCloudiness(oneLocation.getClouds().getCloudiness());
		}
		if (oneLocation.getSnow() != null) {
			weather.setSnowVloumeLastHour(oneLocation.getSnow().getLast1Hour());
		}
		return weather;
	}
	
	public static OneLocationApiRequest modelToRequest(Position position, String appToken, String units) {
		OneLocationApiRequest oneLocation = new OneLocationApiRequest();
		oneLocation.setAppid(appToken);
		oneLocation.setUnits(units);
		oneLocation.setLon(position.getLongitude());
		oneLocation.setLat(position.getLatitude());
		return oneLocation;
	}
}
