package it.unimib.cc.dealer.sensors.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class CoordApiResponse {

	@JsonProperty("lon")
	private Float longitude = null;

	@JsonProperty("lat")
	private Float latitude = null;
	
}
