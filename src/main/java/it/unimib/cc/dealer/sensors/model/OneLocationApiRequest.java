package it.unimib.cc.dealer.sensors.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OneLocationApiRequest {

	public Float lat = null;

	public Float lon = null;

	public String units = null;

	public String appid = null;
}
