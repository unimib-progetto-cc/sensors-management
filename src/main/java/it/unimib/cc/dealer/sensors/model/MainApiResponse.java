package it.unimib.cc.dealer.sensors.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class MainApiResponse {

	@JsonProperty("temp")
	private Float temperature = null;

	@JsonProperty("feels_like")
	private Float feels_like = null;

	@JsonProperty("temp_min")
	private Float temperatureMin = null;

	@JsonProperty("temp_max")
	private Float temperatureMax = null;

	@JsonProperty("pressure")
	private Float pressure = null;

	@JsonProperty("humidity")
	private Integer humidity = null;

	@JsonProperty("sea_level")
	private Float seaLevelPressure = null;

	@JsonProperty("grnd_level")
	private Float groundLevelPressure = null;
}
