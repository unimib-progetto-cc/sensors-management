package it.unimib.cc.dealer.sensors.type;

public class AppConst {

	public final class WeatherMicroserviceEndpoint {
		private WeatherMicroserviceEndpoint() {}
		public static final String TOKEN = "endpoint.microservice.weather.token";
		public static final String PREFIX = "endpoint.microservice.weather.prefix";
		public static final String ONE_LOCATION = "endpoint.microservice.weather.oneLocation";
	}

	public final class WeatherCustomization {
		private WeatherCustomization() {}
		public static final String UNITS = "weather.customization.units";
	}

	public final class CarsManagementMicroserviceEndpoint {
		private CarsManagementMicroserviceEndpoint() {}
		public static final String PREFIX = "endpoint.microservice.carsManagement.prefix";
		public static final String GET_CARS = "endpoint.microservice.carsManagement.getCars";
		public static final String GET_CAR_PREFIX = "endpoint.microservice.carsManagement.getCarPrefix";
	}	
}
