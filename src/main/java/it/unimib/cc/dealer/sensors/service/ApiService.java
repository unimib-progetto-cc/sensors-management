package it.unimib.cc.dealer.sensors.service;

import java.lang.reflect.Field;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import it.unimib.cc.dealer.sensors.factory.WeatherFactory;
import it.unimib.cc.dealer.sensors.model.OneLocationApiRequest;
import it.unimib.cc.dealer.sensors.model.OneLocationApiResponse;
import it.unimib.cc.dealer.sensors.model.Position;
import it.unimib.cc.dealer.sensors.type.AppConst.WeatherCustomization;
import it.unimib.cc.dealer.sensors.type.AppConst.WeatherMicroserviceEndpoint;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ApiService {
	
	@Autowired
	private Environment environment;

	private static <T> ResponseEntity<T> getCall(String url, Object requestModel, Class<T> responseType, HttpHeaders httpHeaders) {
		RestTemplate restTemplate = null;
		ResponseEntity<T> response = null;
		
		try {
			restTemplate = new RestTemplate();
			HttpEntity<Object> httpEntity = new HttpEntity<>(requestModel, httpHeaders);
			
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url);
			for (Field field : requestModel.getClass().getDeclaredFields()) {
	            uriBuilder.queryParam(field.getName(), field.get(requestModel));
	        }
			
			log.info("HTTP URL: {}", uriBuilder.toUriString());
			log.info("HTTP REQUEST: {}", httpEntity);
			log.info("HTTP REQUEST: {}", httpEntity.getBody());			
			response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.POST, httpEntity, responseType);
			log.info("HTTP RESPONSE: {}", response.getBody());
			
			return ((response.getStatusCode() == HttpStatus.OK) ? (response) : (null));
		} 
		catch (Exception e) {
			log.info("Error during microservice call ({}) => {}", url, e);
			return null;
		}
	}

	@SuppressWarnings("unused")
	private static <T> ResponseEntity<T> postCall(String url, Object requestParameters, Class<T> responseType, HttpHeaders httpHeaders) {
		RestTemplate restTemplate = null;
		ResponseEntity<T> response = null;
		try {
			restTemplate = new RestTemplate();
			HttpEntity<Object> httpEntity = new HttpEntity<>(null, httpHeaders);
			
			log.info("HTTP REQUEST: {}", httpEntity);
			log.info("HTTP REQUEST: {}", httpEntity.getBody());
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, responseType);
			log.info("HTTP RESPONSE: {}", response.getBody());
			
			return ((response.getStatusCode() == HttpStatus.OK) ? (response) : (null));
		} 
		catch (Exception e) {
			log.info("Error during microservice call ({}) => {}", url, e);
			return null;
		}
	}

	public OneLocationApiResponse oneLocationWeather(Position position) {
		String url = environment.getProperty(WeatherMicroserviceEndpoint.ONE_LOCATION);
		OneLocationApiRequest requestParams = WeatherFactory.modelToRequest(position,
				environment.getProperty(WeatherMicroserviceEndpoint.TOKEN),
				environment.getProperty(WeatherCustomization.UNITS));
		ResponseEntity<OneLocationApiResponse> response = getCall(url, requestParams, OneLocationApiResponse.class, new HttpHeaders());
		return (response != null ? response.getBody() : null);
	}
}
