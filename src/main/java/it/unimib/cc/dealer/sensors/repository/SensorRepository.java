package it.unimib.cc.dealer.sensors.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.unimib.cc.dealer.sensors.model.Sensor;

public interface SensorRepository extends JpaRepository<Sensor, Long> {
	Optional<Sensor> findById(long sensorId);
	Optional<Sensor> findTopByCarIdOrderByDatetimeDesc(long carId);
	List<Sensor> findByCarId(long carId);
	List<Sensor> findAll();
	List<Sensor> findByDatetimeLessThanEqual(LocalDateTime endDateTime);
	List<Sensor> findByDatetimeGreaterThanEqual(LocalDateTime startDateTime);
	List<Sensor> findByCarIdAndDatetimeLessThanEqual(long carId, LocalDateTime endDateTime);
	List<Sensor> findByCarIdAndDatetimeGreaterThanEqual(long carId, LocalDateTime startDateTime);
	List<Sensor> findByDatetimeBetween(LocalDateTime startDateTime, LocalDateTime eDateTime);
	List<Sensor> findByCarIdAndDatetimeBetween(long carId, LocalDateTime startDateTime, LocalDateTime eDateTime);
}
