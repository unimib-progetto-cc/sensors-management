package it.unimib.cc.dealer.sensors.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "sensor")
@JacksonXmlRootElement(localName = "sensor")
public class Sensor extends RepresentationModel<Sensor> {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="sensor_id")
	@ApiModelProperty(readOnly = true)
    private long id;

    @Column(nullable = false)
    private long carId;

    @Embedded
    private Position position;

    @Embedded
	@ApiModelProperty(readOnly = true)
    private Weather weather;

    @Column(nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@ApiModelProperty(readOnly = true)
    private LocalDateTime datetime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCarId() {
		return carId;
	}

	public void setCarId(long carId) {
		this.carId = carId;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Weather getWeather() {
		return weather;
	}

	public void setWeather(Weather weather) {
		this.weather = weather;
	}

	public LocalDateTime getDatetime() {
		return datetime;
	}

	public void setDatetime(LocalDateTime datetime) {
		this.datetime = datetime;
	}

	@Override
	public String toString() {
		return "Sensor [id=" + id + ", carId=" + carId + ", position=" + position + ", weather=" + weather
				+ ", datetime=" + datetime + "]";
	}
}
